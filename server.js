const Express = require('express');
const app = new Express();
const cors = require('cors');
const path = require('path');
const multer = require('multer');


const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'uploads/');
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + path.extname(file.originalname)); //Appending extension
  }
});

const upload = multer({ storage: storage });

app.use(Express.json({ limit: '100mb', strict: false }));
app.use(cors());

app.post('/upload', upload.single('file'), (req, res) => {
  console.log(req.file);
  res.send('ok');
});


app.listen(1337, () => {

});
