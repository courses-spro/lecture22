import { Upload, message, Button } from 'antd';
import { UploadOutlined } from '@ant-design/icons';
import style from './style.module.css';
// const SubmitUrl = 'https://www.mocky.io/v2/5cc8019d300000980a055e76';
const SubmitUrl = 'http://localhost:1337/upload'
export default function HomePage() {
  const onChange = (info) => {
    if (info.file.status !== 'uploading') {
      console.log(info.file, info.fileList);
    }
    if (info.file.status === 'done') {
      message.success(`${info.file.name} file uploaded successfully`);
    } else if (info.file.status === 'error') {
      message.error(`${info.file.name} file upload failed.`);
    }
  };
  return <div className={style.container}>
    <p> Example of uploading an image </p>
    <Upload name={'file'} action={SubmitUrl} onChange={onChange}>
      <Button icon={<UploadOutlined />}>Click to Upload</Button>
    </Upload>
  </div>
}
